﻿namespace CRUD_application.Core.Model;

/// <summary>
/// Product model class
/// </summary>
public class Product
{
    /// <summary>
    /// Id field
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// Name field
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Article field
    /// </summary>
    public string Article { get; set; }

    /// <summary>
    /// Type field
    /// </summary>
    public string Type { get; set; }

    /// <summary>
    /// Equipment field
    /// </summary>
    public string Equipment { get; set; }
}