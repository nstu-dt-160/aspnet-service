﻿namespace CRUD_application.Core.Repo;

/// <summary>
/// Repo common interface
/// </summary>
/// <typeparam name="T"></typeparam>
public interface IRepoInterface<T>
{
    /// <summary>
    /// Get all items
    /// </summary>
    /// <returns></returns>
    public List<T> GetAll();

    /// <summary>
    /// Get item by id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    public T? GetById(int id);

    /// <summary>
    /// Create item
    /// </summary>
    /// <param name="item"></param>
    /// <returns></returns>
    public T? Create(T item);

    /// <summary>
    /// Update item
    /// </summary>
    /// <param name="item"></param>
    /// <returns></returns>
    public T Update(T item);

    /// <summary>
    /// Delete item
    /// </summary>
    /// <param name="id"></param>
    public void Delete(int id);
}