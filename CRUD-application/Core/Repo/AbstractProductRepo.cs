﻿namespace CRUD_application.Core.Repo;

using System.Data;
using Model;
using Dapper;

public abstract class AbstractProductRepo : IRepoInterface<Product>
{
    /// <summary>
    /// Db connection object
    /// </summary>
    protected readonly IDbConnection DbConnection;

    /// <summary>
    /// Base constructor
    /// </summary>
    /// <param name="dbConnection"></param>
    protected AbstractProductRepo(IDbConnection dbConnection)
    {
        DbConnection = dbConnection;
    }

    public List<Product> GetAll()
    {
        return DbConnection.Query<Product>("SELECT * FROM products").ToList();
    }

    public Product? GetById(int id)
    {
        return DbConnection.Query<Product>("SELECT * FROM products WHERE id = @id", new { id }).FirstOrDefault();
    }

    public Product? Create(Product item)
    {
        var id = DbConnection.Query<int>(
            "INSERT INTO products (name, article, type, equipment) VALUES(@name, @article, @type, @equipment);"
            + "SELECT currval(pg_get_serial_sequence('products','id'))",
            new { item.Name, item.Article, item.Type, item.Equipment }).FirstOrDefault();

        if (id > 0)
        {
            item.Id = id;
            return item;
        }

        return null;
    }

    public Product Update(Product item)
    {
        DbConnection.Query(
            "UPDATE products SET name = @name, article = @article, type = @type, equipment = @equipment "
            + "WHERE id = @id",
            new { item.Name, item.Article, item.Type, item.Equipment, item.Id });

        return item;
    }

    public void Delete(int id)
    {
        DbConnection.Query("DELETE FROM products WHERE id = @id", new { id });
    }

    /// <summary>
    /// Get product by article
    /// </summary>
    /// <param name="article"></param>
    /// <returns></returns>
    public Product? GetByArticle(string article)
    {
        return DbConnection.Query<Product>("SELECT * FROM products WHERE article = @article limit 1", new { article })
            .FirstOrDefault();
    }

    /// <summary>
    /// Search product by text
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    public abstract List<Product> Search(string text);
}