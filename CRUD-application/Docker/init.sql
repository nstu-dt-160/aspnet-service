﻿CREATE TABLE products
(
    id         serial PRIMARY KEY,
    name       VARCHAR(255) NOT NULL,
    article    VARCHAR(255) NOT NULL,
    type       VARCHAR(255) NOT NULL,
    equipment  VARCHAR(255) NOT NULL,
    created_on TIMESTAMP    NOT NULL DEFAULT current_timestamp
);