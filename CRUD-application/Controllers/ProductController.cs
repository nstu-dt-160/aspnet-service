﻿namespace CRUD_application.Controllers;

using Core.Model;
using Core.Repo;
using Microsoft.AspNetCore.Mvc;

[ApiController]
[Route("/api/v1/[controller]")]
public class ProductController : ControllerBase
{
    /// <summary>
    /// Product repo class
    /// </summary>
    private readonly AbstractProductRepo _productRepo;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="productRepo"></param>
    public ProductController(AbstractProductRepo productRepo)
    {
        _productRepo = productRepo;
    }

    /// <summary>
    /// Get all products
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public ActionResult<List<Product>> Get()
    {
        return _productRepo.GetAll();
    }

    /// <summary>
    /// Get product by id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("{id:int}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public ActionResult<Product> GetById(int id)
    {
        var product = _productRepo.GetById(id);

        if (product == null)
        {
            return NotFound();
        }

        return product;
    }

    /// <summary>
    /// Create product
    /// </summary>
    /// <param name="product"></param>
    /// <returns></returns>
    [HttpPost]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public ActionResult<Product> Post(Product product)
    {
        var result = _productRepo.Create(product);

        if (result == null)
        {
            return StatusCode(500);
        }

        return result;
    }

    /// <summary>
    /// Update all product fields
    /// </summary>
    /// <param name="product"></param>
    /// <returns></returns>
    [HttpPut]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public ActionResult<Product> Put(Product product)
    {
        _productRepo.Update(product);

        return product;
    }

    /// <summary>
    /// Delete product
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpDelete("{id:int}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public ActionResult<Product> Delete(int id)
    {
        _productRepo.Delete(id);

        return StatusCode(204);
    }

    /// <summary>
    /// Search product by text
    /// </summary>
    /// <returns></returns>
    [HttpGet("Search/{text}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public ActionResult<List<Product>> Search(string text)
    {
        return _productRepo.Search(text);
    }

    /// <summary>
    /// Get product by article
    /// </summary>
    /// <param name="article"></param>
    /// <returns></returns>
    [HttpGet("Article/{article}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public ActionResult<Product> GetByArticle(string article)
    {
        var result = _productRepo.GetByArticle(article);

        if (result == null)
        {
            return NotFound();
        }

        return result;
    }
}