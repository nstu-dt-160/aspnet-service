﻿Для запуска базы данных необходимо в папке Docker выполнить команду:

```bash
docker-compose up -d
```

База данных доступна по порту 5432. Админ-панель по порту 8888. Данные для подключения:

- Host: localhost
- User: admin
- Password: changeme
- Database: app_db
