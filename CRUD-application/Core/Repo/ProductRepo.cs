﻿namespace CRUD_application.Core.Repo;

using Model;
using Dapper;
using System.Data;

public class ProductRepo : AbstractProductRepo
{
    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="dbConnection"></param>
    public ProductRepo(IDbConnection dbConnection) : base(dbConnection)
    {
    }

    public override List<Product> Search(string text)
    {
        return DbConnection.Query<Product>(
                "SELECT * FROM products WHERE name like @text"
                , new { text = "%" + text + "%" })
            .ToList();
    }
}